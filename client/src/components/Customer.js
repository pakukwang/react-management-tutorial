import React from "react";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";

class Customer extends React.Component {
    render() {
        return(
            <TableRow>
                <TableCell>{this.props.customerId}</TableCell>
                <TableCell><img src={this.props.customerImage} alt = "profile"/></TableCell>
                <TableCell>{this.props.customerNm}</TableCell>
                <TableCell>{this.props.customerBirth}</TableCell>
                <TableCell>{this.props.customerGender}</TableCell>
                <TableCell>{this.props.customerJob}</TableCell>
            </TableRow>
        );
    }
}

/*
{
class CustomerProfile extends React.Component {
    render() {
        return(
            <div>
                <img src={this.props.img} alt = "profile_img" />
                <h2>{this.props.name}({this.props.id})</h2>
            </div>
        );
    }
}

class CustomerInfo extends React.Component {
    render() {
        return(
            <div>
                <p>{this.props.birth}</p>
                <p>{this.props.gender}</p>
                <p>{this.props.job}</p>
            </div>
        );
    }
}
}
*/
export default Customer;

