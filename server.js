const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const port = process.env.PORT || 5000;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

app.get('/api/customers', (req, res) => {
    res.send([
      {
        'id': '1',
        'img': 'https://placeimg.com/128/128/1',
        'name': 'Park', 
        'birth': '891231', 
        'gender': 'M',
        'job': 'Programmer'
      },
      {
        'id': '2',
        'img': 'https://placeimg.com/128/128/2',
        'name': 'Kim', 
        'birth': '920203', 
        'gender': 'W',
        'job': 'Designer'
      },
      {
        'id': '3',
        'img': 'https://placeimg.com/128/128/3',
        'name': 'Lee', 
        'birth': '981130', 
        'gender': 'M',
        'job': 'Project Manager'
      }]);
});

app.listen(port, () => console.log(`Listening on port ${port}`));
